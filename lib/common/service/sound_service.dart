import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_sound/flutter_sound.dart';

class SoundService extends ChangeNotifier {
  SoundService() {
    _init();
  }

  bool _inited = false;
  bool get inited => _inited;

  FlutterSoundPlayer? _bgmPlayer;
  FlutterSoundPlayer? get bgmPlayer => _bgmPlayer;

  Future<Uint8List> _getAssetData(String path) async {
    var asset = await rootBundle.load(path);
    return asset.buffer.asUint8List();
  }

  void _init() {
    _bgmPlayer = FlutterSoundPlayer();
    _bgmPlayer?.openAudioSession().then((value) {
      _inited = true;
      notifyListeners();
    });
  }

  void startBgm() async {
    if (_inited) {
      Uint8List buffer = await _getAssetData('assets/sample/sample.mp4');
      await _bgmPlayer?.startPlayer(
        fromDataBuffer: buffer,
        codec: Codec.aacMP4,
        whenFinished: () {
          notifyListeners();
        },
      );
      notifyListeners();
    }
  }

  Future<void> stopBgm() async {
    if (_bgmPlayer != null && _inited) {
      await _bgmPlayer!.stopPlayer();
      notifyListeners();
    }
  }

  @override
  void dispose() {
    _bgmPlayer?.closeAudioSession();
    _bgmPlayer = null;
    super.dispose();
  }
}
