import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sound_demo/common/service/sound_service.dart';
import 'package:sound_demo/home/views/home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SoundService>(
      create: (context) => SoundService(),
      child: MaterialApp(
        title: 'Sound Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const HomeScreen(),
      ),
    );
  }
}
