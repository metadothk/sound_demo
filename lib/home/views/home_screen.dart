import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sound_demo/common/service/sound_service.dart';
import 'package:sound_demo/home/views/second_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final SoundService soundService = context.watch<SoundService>();
    final bool isPlaying = soundService.bgmPlayer?.isPlaying ?? false;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Center(
        child: Column(
          children: [
            Text('BG Started: $isPlaying'),
            const SizedBox(height: 12),
            IconButton(
              onPressed: () {
                isPlaying ? soundService.stopBgm() : soundService.startBgm();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const SecondScreen(),
                  ),
                );
              },
              icon: Icon(
                isPlaying ? Icons.pause : Icons.play_arrow,
              ),
            )
          ],
        ),
      ),
    );
  }
}
