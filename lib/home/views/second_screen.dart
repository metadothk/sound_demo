import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sound_demo/common/service/sound_service.dart';

class SecondScreen extends StatefulWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second'),
      ),
      body: Center(
        child:
            Text(context.watch<SoundService>().bgmPlayer!.isPlaying.toString()),
      ),
    );
  }
}
